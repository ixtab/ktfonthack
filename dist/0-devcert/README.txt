This file contains the combined (merged) developer keystore from this thread: http://www.mobileread.com/forums/showthread.php?t=152294

All credits go to the original developers, and to PoP for packaging them together. I merely created an installer/uninstaller for them.


To install or uninstall : put the respective .bin file directly on your Kindle (not in any subdirectory), then use Menu > Settings, Menu > Update your Kindle.

