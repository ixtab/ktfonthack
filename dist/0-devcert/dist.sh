#!/bin/sh

DIST=combined-dev-certs-20121002

rm *.zip *.bin

kindletool create ota2 -d k5w -d k5g -d k5gb -d k5u -d pw -d pwg -d pwgb -C src/install/ update_${DIST}_install.bin
kindletool create ota2 -d k5w -d k5g -d k5gb -d k5u -d pw -d pwg -d pwgb -C src/uninstall/ update_${DIST}_uninstall.bin
zip -9 -r ${DIST}.zip *.bin README.txt
