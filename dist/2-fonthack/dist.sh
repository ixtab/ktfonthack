#!/bin/sh

DIST=fonthack-1.3.2
KINDLE_TOOL="java -jar $HOME/kindle-touch/localization/kindle-touch-l10n/tool/KTFrameWorkLocale/kt-l10n.jar kindletool -f"

rm *.bin *.zip
cp ../../fonthack.azw2 src/install/
$KINDLE_TOOL -s src/install/ -t update_${DIST}_install.bin
$KINDLE_TOOL -s src/uninstall/ -t update_${DIST}_uninstall.bin
zip -9 -r ${DIST}.zip *.bin *.zip src README.txt

