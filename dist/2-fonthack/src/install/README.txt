Kindle Touch Font Hack
======================

This directory contains the necessary files to make the "Font Hack" application work.

It contains various kinds of information, and I'll shortly explain each of them below.

Font Directories
----------------
Every directory in this folder is considered to contain a single font. The directory name
is the font name. Font names may ONLY contain letters (A-Z,a-z), numbers (0-9), spaces (" "),
and the symbols "-" or "_". For instance, if you want to make the font "My Special Font" available
on the Kindle, then you must create the directory "My Special Font", and that directory
must contain the files "My Special Font Regular.ttf", "My Special Font Italic.ttf",
"My Special Font Bold.ttf", and "My Special Font Bold Italic.ttf". Note that the folder name
and the .ttf file "prefixes" must match EXACTLY. Both are case-sensitive. To remove a font,
simply delete the font directory *AND* the relevant metadata (see below).

Note that very few fonts (such as the Calibri font from Windows 7) have been found to simply
not work with the Kindle.

Configuration Metadata
----------------------
Inside individual font directories, you may find the files "fonts.scale" and "fonts.dir".
These are system files which can be generated on the Kindle itself. In other words:
don't worry if they don't exist, and DO NOT distribute these files.

Inside the fonthack directory, you will find a file called "00-FontName.conf" for each
font that has been installed and initialized on your system. This file is generated
automatically if it doesn't exist upon font initialization. For particular fonts, you
may need to modify this file to provide proper results. If changes are required, you
can also redistribute this (changed) file along with your fonts.

Inside the fonthack directory, you will also find files named "99-serif.conf" (for
instance). These files appear if you have overridden system fonts. Do not change, delete,
or redistribute these files unless you know what you are doing.

Translation Files
-----------------
The Font Hack application can be easily localized by copying "translation-*.properties" files
into the fonthack directory. This is best shown using an example. Suppose your KT is 
localized to "German (Germany)" (= "Deutsch (Deutschland)"). The application will then look
for a file named "translation-de_DE.properties" inside the fonthack directory.
- If that file is not found, no localization is attempted.
- If that file is found AND it is not empty (not of size 0), it will be used for localization.
- If that file is found AND it is empty (size 0), it will be overwritten to contain a
  localization with the default (english) strings. This is meant as a way to bootstrap
  translations: simply modify the file, and restart the application, to get it translated.

Translation files are expected to be in UTF-8 format.



For further information, please see http://www.mobileread.com/forums/showthread.php?t=168765

-- ixtab.
