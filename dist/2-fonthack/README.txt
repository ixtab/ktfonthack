  ==========================
  = Kindle Touch Font Hack =
  ==========================


This application allows you to change the fonts used on your Kindle Touch.

In order to use this application, you will need to have BOTH the Developer
certificate, and the Kindlet Jailbreak installed.

Don't worry, the instructions below will lead you through the installation.

INSTALLATION
============

1-a. Plug your Kindle to your computer.
1-b. Copy the update_fonthack-*_install.bin file to the root directory of your Kindle.
1-c. Unplug the Kindle from your computer, then select Menu > Settings, Menu > Update Your Kindle.

2-a. After the Kindle has restarted, launch "Font Hack" from your home screen. 
2-b. IF you receive a message about "Test Kindle", unauthorized, developer etc., continue with step 3-a.
2-c. IF you receive a message about the Kindlet jailbreak not being installed, continue with step 4-a.
2-d. Continue with step 5.

3-a. Install the developer certificate bundle from combined-dev-certs-*.zip
3-b. After you have successfully installed the bundle, start again from step 2-a.

4-a. Install the Kindlet jailbreak from kindlet-jailbreak-*.zip
4-b. After you have successfully installed the bundle, start again from step 2-a.

5. The font hack is successfully installed, however no actual fonts have been installed yet.
5.1. You can install fonts in various ways:
     - install them by yourself according to the README file contained in the "fonthack" directory
     - wait for someone else to create a package for you, and install that package.
5.2. Similarly, you can get the application localized (if need be) by either creating your own
     translation file, or by hoping for someone else to provide it.


UNINSTALLATION
==============

Invoke the abovementioned installers in reverse order. Two notes:

- the Font Hack uninstaller will NOT remove the "fonthack" directory. You can safely remove it by
  yourself after uninstalling the application.

- the Font Hack uninstaller WILL run for a longer time (minutes, not seconds) , because it will
  reset the font cache on uninstallation.


USAGE TIPS
==========

* Fonts that are marked as "invalid" are not considered. Either remove the font directory, or fix the
  names of the contained files.

* Especially after you batch-install fonts, you may have a lot of uninitialized fonts. You need to
  initialize each of them separately, however you can defer the required cache rebuild until you
  initialized all of them.

* Similarly, if you make changes to the font types (serif/sans serif/monospace/condensed) assignments,
  be it new assignments or resets, you can perform all of these at once. A single restart of the
  framework will pick up all changes.

* In some circumstances, not all fonts display properly at all sizes. You may get entirely blank "text"
  at times. The reason seems to be the severely limited memory of the Kindle. Also, some fonts are
  more often affected than others. There is no way to fix this.

* Some few fonts (such as the Calibri font from Windows 7) have been found to simply not work at all
  on the Kindle.
