This file contains a "Jailbreak" which allows Kindlets to break out of their restricted environment.

Note that this is a DIFFERENT Jailbreak than the device jailbreak, and it needs to be installed IN ADDITION to the device jailbreak.

Installation/uninstallation is as usual: copy the respective update_*.bin to your Kindle, use Menu > Settings, Menu > Update Your Kindle.
