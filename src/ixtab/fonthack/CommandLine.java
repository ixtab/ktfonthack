package ixtab.fonthack;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CommandLine {

	public static final int STDOUT = 0;
	public static final int STDERR = 1;
	
	public static class Result {
		private final InputStream[] streams;;
		private final String[][] stringArrays = new String[2][];
		public final Process process;
		
		public Result(Process process) {
			this.process = process;
			streams = new InputStream[] {process.getInputStream(), process.getErrorStream()};
		}

		public String[] getAsStringArray(int index) throws IOException {
			if (stringArrays[index] == null) {
					fillStringArray(index);
				return stringArrays[index];
			}
			return stringArrays[index];
		}

		private void fillStringArray(int index) throws IOException {
			String[] result = readFromStream(streams[index]);
			stringArrays[index] = result;
		}

		private String[] readFromStream(InputStream stream) throws IOException {
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			List<String> lines = new ArrayList<String>();
			for (String line = reader.readLine(); line != null; line = reader.readLine()) {
				lines.add(line);
			}
			return lines.toArray(new String[lines.size()]);
		}

		public InputStream getStdOutStream() {
			return streams[0];
		}
		
	}
	
	public static Result exec(boolean block, String... cmd) throws IOException {
		return exec(null, block, cmd);
	}
	
	public static Result exec(File cwd, boolean block, String... cmd) throws IOException {
		Process process = Runtime.getRuntime().exec(cmd, null, cwd);
		if (block) {
			try {
				int result = process.waitFor();
				if (0 != result) {
					throw new RuntimeException("Expected process result 0, got "+ result);
				}
			} catch (Throwable t) {
				throw new RuntimeException(t);
			}
		}
		Result r = new Result(process);
		return r;
	}

}
