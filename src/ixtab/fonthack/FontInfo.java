package ixtab.fonthack;

import java.awt.Font;

public abstract class FontInfo implements Comparable<FontInfo> {
		
	public static class Variant {
		public final String description;
		public final int style;
		public Variant(String description, int style) {
			super();
			this.description = description;
			this.style = style;
		}
	}
	
	public static final String[] VARIANT_NAMES = new String[] {
		"Regular", "Bold", "Italic", "Bold Italic"
	};
	
	public static final Variant[] VARIANTS = new Variant[] {
		new Variant("Regular", Font.PLAIN),
		new Variant("Bold", Font.BOLD),
		new Variant("Italic", Font.ITALIC),
		new Variant("Bold Italic", Font.BOLD | Font.ITALIC)
	};
	
	@Override
	public int compareTo(FontInfo o) {
		return getName().compareTo(o.getName());
	}

	public abstract String getName();

	@Override
	public String toString() {
		// easy fix to make it look better on the Kindle.
		return " " + getName() + " ";
	}

	public abstract boolean isHack();
	public abstract boolean isSystem();
	public abstract boolean isValid();
	public abstract boolean isInitialized();
	
	

}
