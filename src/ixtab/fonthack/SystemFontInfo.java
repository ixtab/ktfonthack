package ixtab.fonthack;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class SystemFontInfo extends FontInfo {
	private static final String INTERNAL_CONDENSED = "condensed";

	private static final String INTERNAL_MONOSPACE = "monospace";

	private static final String INTERNAL_SANSSERIF = "sansserif";

	private static final String INTERNAL_SERIF = "serif";

	public static final String MARKER = "[S]";

	public static class Descriptor {
		public final String internalName;
		public final String javaName;
		public final String defaultFont;
		public Descriptor(String internalName, String javaName,
				String defaultFont) {
			super();
			this.internalName = internalName;
			this.javaName = javaName;
			this.defaultFont = defaultFont;
		}
		
		public boolean needsTwoFiles() {
			return internalName.equals(INTERNAL_CONDENSED) || internalName.equals(INTERNAL_SERIF);
		}
		
	}
	
	public static final Descriptor[] DESCRIPTORS = new Descriptor[] {
		new Descriptor(INTERNAL_SERIF, "Serif", "Caecilia Regular"),
		new Descriptor(INTERNAL_SANSSERIF, "Sans Serif", "Helvetica Neue LT"),
		new Descriptor(INTERNAL_MONOSPACE, "Monospace", "KindleBlackboxC"),
		new Descriptor(INTERNAL_CONDENSED, "condensed", "condensed"),
	};
	
	private static final Map<String, String> cache = new TreeMap<String, String>();
	
	static void clearCache() {
		synchronized (cache) {
			cache.clear();
		}
	}
	public static String retrieve(String styleName) {
		synchronized (cache) {
			if (cache.containsKey(styleName)) {
				return cache.get(styleName);
			}
			String[] commandLine = new String[] { "/usr/bin/fc-match", "-f", "%{family}\\n", styleName};
			try {
				String[] result = CommandLine.exec(true, commandLine).getAsStringArray(CommandLine.STDOUT);
				if (result.length != 1) {
					throw new IllegalStateException("Expected exactly one result for "+commandLine+", got " + result.length );
				}
				cache.put(styleName, result[0]);
				return result[0];
			} catch (IOException io) {
				throw new RuntimeException(io);
			}
			
		}
	}


	private final String name;
	
	public SystemFontInfo(String name) {
		super();
		this.name = name;
	}

	@Override
	public boolean isInitialized() {
		return true;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isHack() {
		return false;
	}

	@Override
	public String toString() {
		return " "+ MARKER + super.toString();
	}

	@Override
	public boolean isSystem() {
		return true;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	public static boolean isSystemFontChanged(String javaName) {
		for (Descriptor desc: DESCRIPTORS) {
			if (desc.javaName.equals(javaName)) {
				String current = retrieve(javaName);
				return (!desc.defaultFont.equals(current));
			}
		}
		return false;
	}

	public static boolean reset(SystemFontInfo fi) {
		Descriptor desc = findDescriptorFor(fi);
		if (desc != null) {
			return reset(desc);
		}
		return false;
	}

	private static boolean reset(Descriptor desc) {
		boolean ok = FontConfig.removeOverride(desc);
		if (ok) {
			clearCache();
		}
		return ok;
	}

	private static Descriptor findDescriptorFor(SystemFontInfo fi) {
		if (fi == null) return null;
		for (Descriptor desc: DESCRIPTORS) {
			if (desc.javaName.equals(fi.getName())) {
				return desc;
			}
		}
		return null;
	}

}
