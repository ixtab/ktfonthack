package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources._error_message;
import static ixtab.fonthack.resources.FontHackResources._error_title;
import static ixtab.fonthack.resources.FontHackResources.i18n;
import ixtab.fonthack.SystemFontInfo.Descriptor;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.amazon.kindle.kindlet.ui.KOptionPane;

import edu.emory.mathcs.backport.java.util.Collections;


public final class InfoProvider {

	public static final String[] METADATA_FILES = new String[] {"fonts.dir", "fonts.scale"};
	
	public static final int LISTMODE_HACK = 1;
	public static final int LISTMODE_USABLE = 2;
	
	public static final String MKFONTSCALE = "/usr/bin/mkfontscale";
	public static final String MKFONTDIR = "/usr/bin/mkfontdir";
	
	private static InfoProvider instance;
	
	private final List<FontInfo> fonts;
	
	
	public static InfoProvider getInstance() {
		if (instance == null) {
			try {
				instance = new InfoProvider(new File("/mnt/us/fonthack/"));
			} catch (IllegalArgumentException e) {
				return null;
			}
		}
		return instance;
	}
	
	private InfoProvider(File fontsDirectory) {
		if (!fontsDirectory.isDirectory() && !fontsDirectory.mkdirs()) {
			throw new IllegalArgumentException();
		}
		fonts = findFontsIn(fontsDirectory);
	}
	
	private List<FontInfo> findFontsIn(File fontsDirectory) {
		List<FontInfo> list = new ArrayList<FontInfo>();
		if (fontsDirectory != null && fontsDirectory.isDirectory()) {
			for (File file: fontsDirectory.listFiles()) {
				if (file.isDirectory()) {
					list.add(new HackFontInfo(file));
				}
			}
		}
		Collections.sort(list);
		// now add system fonts to the beginning of the list
		for (int i=SystemFontInfo.DESCRIPTORS.length - 1; i >= 0; --i) {
			list.add(0, new SystemFontInfo(SystemFontInfo.DESCRIPTORS[i].javaName));
		}
		return list;
	}


//	public static class DefaultProvider {
//		private static InfoProvider provider = new KindleInfoProvider();
//		
//		public static InfoProvider get() {
//			return provider;
//		}
//		
//		public static void set(InfoProvider provider) {
//			DefaultProvider.provider = provider;
//		}
//	}

	public FontInfo[] getFontInfoArray(int mode) {
		List<FontInfo> result = createFontList(mode);
		return result.toArray(new FontInfo[result.size()]);
	}

	private List<FontInfo> createFontList(int mode) {
		List<FontInfo> result = new ArrayList<FontInfo>();
		for (FontInfo info: fonts) {
			if (mode == LISTMODE_USABLE) {
				if (info.isInitialized()) {
					result.add(info);
				}
			} else if (mode == LISTMODE_HACK) {
				if  (info.isHack()) {
					result.add(info);
				}
			} else {
				throw new IllegalArgumentException("unknown list mode "+mode);
			}
		}
		return result;
	}


	public void resetSystemFont(Component parentView, FontInfo selectedFont) {
		if (selectedFont == null || !(selectedFont instanceof SystemFontInfo)) {
			KOptionPane.showMessageDialog(parentView, "Oops. This should not have happened. selectedFont == "+selectedFont, "Ooops");
			return;
		}
		SystemFontInfo fi = (SystemFontInfo) selectedFont;
		if (SystemFontInfo.reset(fi)) {
			new FontEvent(FontEvent.Listener.RESTART_REQUIRED).post();
			new FontSelectionChangedEvent(null, null, null, fi).post();
		} else {
			KOptionPane.showMessageDialog(parentView, i18n(_error_message), i18n(_error_title));
		}
	}

	public boolean initializeHackFont(Component view, HackFontInfo font) {
		File fontDir = font.getDirectory();
		
		try {
			CommandLine.exec(fontDir, true, MKFONTSCALE);
			CommandLine.exec(fontDir, true, MKFONTDIR);
			File fontConfig = font.getConfigurationFile();
			if (!fontConfig.exists()) {
				return FontConfig.createDefaultInclusionFile(font);
			}
			return true;
		} catch (IOException e) {
			String error = StackTrace.get(e);
			KOptionPane.showMessageDialog(view, error, i18n(_error_title));
			return false;
		}
	}

	public void overrideWithHackFont(Component view, Descriptor system,
			HackFontInfo font) {
		try {
			boolean ok = FontConfig.createOverride(system, font);
			if (ok) {
				SystemFontInfo.clearCache();
				new FontEvent(FontEvent.Listener.RESTART_REQUIRED).post();
				new FontSelectionChangedEvent(null, null, null, font).post();
			}
		} catch (IOException e) {
			String error = StackTrace.get(e);
			KOptionPane.showMessageDialog(view, error, i18n(_error_title));
		}
	}


	public boolean isSystemFontChanged(String javaName) {
		return SystemFontInfo.isSystemFontChanged(javaName);
	}


}
