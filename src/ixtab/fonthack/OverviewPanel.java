package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources.*;
import ixtab.fonthack.SystemFontInfo.Descriptor;
import ixtab.fonthack.resources.Icons;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.amazon.kindle.kindlet.ui.KOptionPane;

public class OverviewPanel extends JPanel implements FontEvent.Listener {
	public static String getDescription() {
		return i18n(_overviewpanel_description);
	}
	

	private static final int FONTS_PER_PAGE = 8;
	public static final int FONT_SIZE = 14;
	private static final long serialVersionUID = -6161295149973973113L;

	private final InfoProvider infoProvider;
	private JPanel[] fontPanels;
	private final JPanel centerPanel;
	
	private final JButton previousPageButton;
	private final JButton nextPageButton;
	private int currentPage = 0;

	public OverviewPanel(InfoProvider fontInfoProvider) {
		this.infoProvider = fontInfoProvider;
		
		centerPanel = new JPanel(new CardLayout());
		
		previousPageButton = new JButton("<<");
		nextPageButton = new JButton(">>");
		
		ActionListener buttonActions = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int offset = e.getSource().equals(previousPageButton) ? -1 : 1;
				turnPage(offset);
			}
		};
		previousPageButton.addActionListener(buttonActions);
		nextPageButton.addActionListener(buttonActions);
		
		reinitUI();
		FontEvent.addListener(this);
	}

	// offset should be -1, 0, or 1.
	private void turnPage(int offset) {
		int newPage = currentPage + offset;
		int totalPages = centerPanel.getComponentCount();
		
		if (newPage < 0) newPage = 0;
		else if (newPage >= totalPages) {
			newPage = totalPages -1;
		}
		previousPageButton.setEnabled(newPage != 0);
		nextPageButton.setEnabled(newPage != totalPages-1);
		
		CardLayout card = (CardLayout) centerPanel.getLayout();
		if (newPage < currentPage) {
			card.previous(centerPanel);
		} else if (newPage > currentPage) {
			card.next(centerPanel);
		}
		
		currentPage = newPage;
		this.validate();
	}

	public void reinitUI() {
		fontPanels = buildFontPanels(infoProvider.getFontInfoArray(InfoProvider.LISTMODE_HACK));
		rebuildLayout();
		// only to initialize button status
		turnPage(0);
	}

	private JPanel[] buildFontPanels(FontInfo[] fontInfos) {
		Map<String, String> systemFontMap = buildSystemFontMap();
		JPanel[] panels = new JPanel[fontInfos.length];
		for (int i=0; i < fontInfos.length; ++i) {
			panels[i] = buildFontPanel(fontInfos[i], systemFontMap);
		}
		if (panels.length == 0) {
			JPanel infoPanel = new JPanel();
			addIcon(infoPanel, Icons.UNINITIALIZED);
			JLabel info = new JLabel(i18n(_overviewpanel_no_fonts_label));
			infoPanel.add(info);
			panels = new JPanel[] {infoPanel};
		}
		return panels;
	}

	private Map<String, String> buildSystemFontMap() {
		Map<String, String> result = new TreeMap<String, String>();
		for (Descriptor descriptor: SystemFontInfo.DESCRIPTORS) {
			String style = descriptor.internalName;
			String font = SystemFontInfo.retrieve(style);
			result.put(style, font);
		}
		return result;
	}

	private JPanel buildFontPanel(final FontInfo fontInfo, Map<String, String> systemFonts) {
		JPanel result = new JPanel(new BorderLayout());
		result.add(buildIconsPanel(fontInfo, systemFonts), BorderLayout.EAST);
		
		Font font = new Font(fontInfo.getName(), Font.PLAIN, FONT_SIZE);
		String description = fontInfo.getName();
//		if (!fontInfo.getName().equals("HelvXetica")) {
//			description = "WTF? " + font;
//		}
		final JLabel label = new JLabel(description);
//		// FIXME
//		if (fontInfo.getName().equals("Helvetica")) {
//			throw new RuntimeException(""+this.getGraphics());
//		} else {
			label.setFont(font);
//		}
		result.add(label, BorderLayout.CENTER);
		result.add(new JLabel(""), BorderLayout.WEST);
		
		result.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				String font = fontInfo.getName();
				if (!fontInfo.isValid()) {
					String title = i18n(_overviewpanel_invalid_title);
					String message = new MessageFormat(i18n(_overviewpanel_invalid_message)).format(new Object[] {font} );
					KOptionPane.showMessageDialog(OverviewPanel.this, message, title);
				} else if (!fontInfo.isInitialized()) {
					String title = i18n(_overviewpanel_uninitialized_title);
					String message = new MessageFormat(i18n(_overviewpanel_uninitialized_message)).format(new Object[] {font} );
					int choice = KOptionPane.showConfirmDialog(OverviewPanel.this, message, title, KOptionPane.NO_YES_OPTIONS);
					if (choice == KOptionPane.YES_OPTION) {
						if (fontInfo instanceof HackFontInfo) {
							if (infoProvider.initializeHackFont(OverviewPanel.this, (HackFontInfo) fontInfo)) {
								new FontEvent(FontEvent.Listener.REBUILD_REQUIRED).post();
							}
						} else {
							throw new IllegalStateException("This shouldn't have happened.");
						}
					}
				}
				else if (fontInfo.isInitialized()) {
					new FontSelectionChangedEvent(label, null, null, fontInfo).post();
				}
			}
			
		});
		return result;
	}
	
	private JPanel buildIconsPanel(FontInfo fontInfo, Map<String, String> systemFonts) {
		JPanel icons =new JPanel();
		if (!fontInfo.isValid()) {
			addIcon(icons, Icons.INVALID);
		} else if (!fontInfo.isInitialized()) {
			addIcon(icons, Icons.UNINITIALIZED);
		} else {
			for (Map.Entry<String, String> entry: systemFonts.entrySet()) {
				if (entry.getValue().equals(fontInfo.getName())) {
					addIcon(icons, entry.getKey());
				}
			}
		}
		return icons;
	}

	public void addIcon(JPanel icons, String iconShortName) {
		icons.add(new JLabel(new ImageIcon(Icons.getUrl(iconShortName+".png"))));
	}

	private void rebuildLayout() {
		this.removeAll();
		currentPage = 0;
		
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx  = 1.0;
        gbc.weighty  = 1.0;

		
		centerPanel.removeAll();
        JPanel pagePanel = createNewPagePanel();
		
        for (int i=0; i < fontPanels.length; ++i) {
        	if (i % FONTS_PER_PAGE == 0 && i != 0) {
        		pagePanel =createNewPagePanel();
        	}
        	pagePanel.add(fontPanels[i], gbc);
        }
        
		
		
        this.setLayout(new BorderLayout());
		this.add(centerPanel, BorderLayout.CENTER);
		
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridBagLayout());
		bottom.add(previousPageButton, gbc);//, BorderLayout.WEST);
		gbc.gridx = 1;
		bottom.add(nextPageButton,gbc);//, BorderLayout.EAST);
		this.add(bottom, BorderLayout.SOUTH);
		
		// this is actually used as a simple way to get some padding around
		// the edges.
		this.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
	}

	private JPanel createNewPagePanel() {
		JPanel pagePanel = new JPanel(new GridBagLayout());
        centerPanel.add(pagePanel, "DUMMY");//+System.nanoTime());
		return pagePanel;
	}

	@Override
	public void onFontEvent(FontEvent event) {
		if ((event.type & RESTART_REQUIRED) == RESTART_REQUIRED || (event.type & REBUILD_DONE) == REBUILD_DONE ) {
			reinitUI();
		}
	}

}
