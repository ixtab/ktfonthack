package ixtab.fonthack;

import java.util.ArrayList;

public class FontEvent {
	
	public interface Listener {
		public static final int REBUILD_REQUIRED = 1;
		public static final int RESTART_REQUIRED = 2;
		public static final int REBUILD_DONE = 4;
		public static final int SHOW_SYSTEM_TAB = 8;
		
		void onFontEvent(FontEvent event);
	}
	
	public final int type;
	
	public FontEvent(int type) {
		this.type = type;
	}
	
	public void post() {
		dispatch(this);
	}
	
	private static final ArrayList<Listener> listeners = new ArrayList<Listener>();
	private static void dispatch(FontEvent event) {
		for (Listener listener: listeners) {
			listener.onFontEvent(event);
		}
	}
	
	public static void addListener(Listener listener) {
		listeners.add(listener);
	}
}
