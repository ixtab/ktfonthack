package ixtab.fonthack;

import ixtab.jailbreak.Jailbreak;

import java.security.AllPermission;

public class FontHackJailbreak extends Jailbreak {

//	@Override
//	public boolean enable() {
//		boolean ok = super.enable();
//		if (ok) {
////			ok &= getContext().requestPermission(new FilePermission("/mnt/us/fonthack", "read,write,delete"));
////			ok &= getContext().requestPermission(new FilePermission("/mnt/us/fonthack/-", "read,write,delete"));
////			ok &= getContext().requestPermission(new FilePermission("/usr/bin/fc-match", "execute"));
////			ok &= getContext().requestPermission(new AllPermission());
//		}
//		return ok;
//	}
	
	public boolean requestPermissions() {
		boolean ok  = getContext().requestPermission(new AllPermission());
		return ok;
	}

}
