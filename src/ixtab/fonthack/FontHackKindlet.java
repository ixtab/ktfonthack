package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources._error_message;
import static ixtab.fonthack.resources.FontHackResources._error_title;
import static ixtab.fonthack.resources.FontHackResources._kindlet_jailbreak_unsuccessful_message;
import static ixtab.fonthack.resources.FontHackResources._kindlet_jailbreak_unsuccessful_title;
import static ixtab.fonthack.resources.FontHackResources.i18n;
import ixtab.jailbreak.Jailbreak;
import ixtab.jailbreak.SuicidalKindlet;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KOptionPane;

public class FontHackKindlet extends SuicidalKindlet {

	private KindletContext context;
	private JLabel centerLabel;
	
	@Override
	protected Jailbreak instantiateJailbreak() {
		return new FontHackJailbreak();
	}


	@Override
	public void onCreate(KindletContext kindletContext) {
		this.context = kindletContext;
		//final Container root = context.getRootContainer();
		String startupMessage = Metadata.SPLASH_TITLE;
		this.centerLabel = new JLabel(startupMessage);
		initUi();
		
		/* The reason for this is not only that it allows for a splash screen.
		 * More importantly, the initialization may take quite some time, and
		 * if we put everything directly into onCreate(), startup may time out,
		 * and fail to launch the application.
		 */
		new Thread() {
			@Override
			public void run() {
				try {
					while (!(context.getRootContainer().isValid() && context.getRootContainer().isVisible())) {
						Thread.sleep(100);
					}
					// splash screen
					Thread.sleep(3500);
				} catch (Exception e) {};
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (jailbreak.isAvailable()) {
							if (((FontHackJailbreak)jailbreak).requestPermissions()) {
								InfoProvider provider = InfoProvider.getInstance();
								if (provider == null) {
									setCentralMessage(i18n(_error_message));
									String error = i18n(_error_message);
									KOptionPane.showMessageDialog(context.getRootContainer(), error, i18n(_error_title));
								}
								else {
									context.getRootContainer().removeAll();
									FontHack fh = new FontHack(context.getRootContainer(), provider);
									fh.create();
								}
							} else {
								setCentralMessage(i18n(_kindlet_jailbreak_unsuccessful_title));
								String error = i18n(_kindlet_jailbreak_unsuccessful_message);
								KOptionPane.showMessageDialog(context.getRootContainer(), error, i18n(_kindlet_jailbreak_unsuccessful_title));
							}
						} else {
							// of course... there is no jailbreak, so also no localization.
							String title = "Kindlet Jailbreak Required";
							String message = "This application requires the Kindlet Jailbreak to be installed. This is an additional jailbreak that must be installed on top of the Device Jailbreak, in order to allow Kindlets to get the required permissions. Please install the Kindlet Jailbreak before using this application.";
							setCentralMessage(title);
							KOptionPane.showMessageDialog(context.getRootContainer(), message, title);
						}
					}
				});
			}
		}.start();
	}


	private void setCentralMessage(String centered) {
		centerLabel.setText(centered);
	}

	
	private void initUi() {
		Container pane = context.getRootContainer();
		pane.removeAll();
		centerLabel.setFont(new Font(centerLabel.getFont().getName(), Font.BOLD, centerLabel.getFont().getSize() + 6));
		
		pane.setLayout(new GridBagLayout());
		
		// I still don't understand how GridBagLayout really works, but this centers the message.
		
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.fill |= GridBagConstraints.VERTICAL;
        gbc.weightx  = 1.0;
        gbc.weighty  = 1.0;
        
		pane.add(centerLabel, gbc);
	}

	

}