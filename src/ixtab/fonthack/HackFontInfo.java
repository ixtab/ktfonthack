package ixtab.fonthack;

import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.emory.mathcs.backport.java.util.Arrays;
import edu.emory.mathcs.backport.java.util.Collections;

public class HackFontInfo extends FontInfo {
	public static class Variant {
		public final String description;
		public final int style;
		public Variant(String description, int style) {
			super();
			this.description = description;
			this.style = style;
		}
	}
	
	public static final String[] VARIANT_NAMES = new String[] {
		"Regular", "Bold", "Italic", "Bold Italic"
	};
	
	public static final Variant[] VARIANTS = new Variant[] {
		new Variant("Regular", Font.PLAIN),
		new Variant("Bold", Font.BOLD),
		new Variant("Italic", Font.ITALIC),
		new Variant("Bold Italic", Font.BOLD | Font.ITALIC)
	};
	
	public static final File FONTINFO_DIRECTORY = new File("/mnt/us/fonthack/");
	
	private final File directory;
	private boolean initialized;
	private final boolean isValid;
	
	public static List<FontInfo> list() {
		return list(FONTINFO_DIRECTORY);
	}
	
	public static List<FontInfo> list(File directory) {
		List<FontInfo> list = new ArrayList<FontInfo>();
		if (directory != null && directory.isDirectory()) {
			for (File file: directory.listFiles()) {
				if (file.isDirectory()) {
					list.add(new HackFontInfo(file));
				}
			}
		}
		Collections.sort(list);
		return list;
	}
	
	public HackFontInfo(File directory) {
		this.directory = directory;
		isValid = isNameValid() && areAllRequiredFilesPresent();
		if (isValid) {
			initialized = areMetadataFilesPresent() && isConfigFilePresent();
		}
	}

	private boolean isNameValid() {
		// unfortunately, regexes cannot be used on the Kindle, so we
		// do it manually.
		for (char c: directory.getName().toCharArray()) {
			if (c >= 'A' && c <= 'Z') continue;
			if (c >= 'a' && c <= 'z') continue;
			if (c >= '0' && c <= '9') continue;
			if (c == ' ' || c == '_' || c == '-') continue;
			return false;
		}
		return true;
	}

	private boolean areAllRequiredFilesPresent() {
		return areFilesPresent(qualify(VARIANT_NAMES), directory.getName()+" ");
	}

	private String[] qualify(String[] names) {
		String[] qualified = new String[names.length];
		for (int i=0; i < names.length; ++i) {
			qualified[i] = names[i]+".ttf";
		}
		return qualified;
	}

	private boolean areMetadataFilesPresent() {
		return areFilesPresent(InfoProvider.METADATA_FILES, "");
	}

	private boolean isConfigFilePresent() {
		File configFile = getConfigurationFile();
		return configFile.isFile();
	}

	public File getConfigurationFile() {
		String filename = directory.getParent();
		if (!filename.endsWith("/")) filename += "/";
		filename += "00-"+directory.getName()+".conf";
		File configFile = new File(filename);
		return configFile;
	}

	public boolean areFilesPresent(String[] mustHave, String prefix) {
		@SuppressWarnings("unchecked")
		List<String> files = Arrays.asList(directory.list());
		Collections.sort(files);
		for (String required: mustHave) {
			String file = prefix + required;
			if (!files.contains(file)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isInitialized() {
		return initialized;
	}
	
	public boolean initialize() {
		if (isInitialized()) return true;
		if (!isValid) return false;
		
		initialized = areMetadataFilesPresent();
		return initialized;
	}

	public String getName() {
		return directory.getName();
	}

	@Override
	public boolean isHack() {
		return true;
	}

	@Override
	public boolean isSystem() {
		return false;
	}

	@Override
	public boolean isValid() {
		return isValid;
	}

	public File getDirectory() {
		return directory;
	}
}
