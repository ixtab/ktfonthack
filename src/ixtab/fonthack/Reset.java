package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources._reset_message;
import static ixtab.fonthack.resources.FontHackResources._reset_title;
import static ixtab.fonthack.resources.FontHackResources.i18n;

import java.awt.Component;
import java.io.IOException;

import com.amazon.kindle.kindlet.ui.KOptionPane;

public class Reset {
	public static void perform(Component parentView) {
		int choice = KOptionPane.showConfirmDialog(parentView, i18n(_reset_message), i18n(_reset_title), KOptionPane.CANCEL_OK_OPTION);
		if (choice == KOptionPane.OK_OPTION) {
			try {
				CommandLine.exec(true, "/usr/bin/killall","cvm");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
