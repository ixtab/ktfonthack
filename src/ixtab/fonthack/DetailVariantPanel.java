package ixtab.fonthack;

import ixtab.fonthack.FontInfo.Variant;
import static ixtab.fonthack.resources.FontHackResources.*;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class DetailVariantPanel extends JPanel implements FontSelectionChangedEvent.Listener {

	private static final long serialVersionUID = 4473517363380155583L;
	private final JLabel sampleLabel = new JLabel();
	private final int fontStyle;
	private String fontName = null;
	public static final String DEFAULT_SAMPLE_TEXT = i18n(_detailspanel_sampleText);
	private String sampleText = DEFAULT_SAMPLE_TEXT;
	private int size = OverviewPanel.FONT_SIZE;

	public DetailVariantPanel(Variant variant) {
		this.fontStyle = variant.style;
		setLayout(new BorderLayout());
		add(new JLabel(" " + variant.description), BorderLayout.NORTH);
		add(sampleLabel, BorderLayout.SOUTH);
		FontSelectionChangedEvent.addListener(this);
		updateSampleLabel();
	}
	
	public void setText(String text) {
		sampleLabel.setText(text);
	}
	
	@Override
	public void onFontSelectionChangedEvent(FontSelectionChangedEvent event) {
		if (event.size != null) {
			this.size = event.size.intValue();
		}
		if (event.fontInfo != null) {
			this.fontName = event.fontInfo.getName();
		}
		if (event.sampleText != null) {
			this.sampleText = event.sampleText;
		}
		updateSampleLabel();
	}
	
	private void updateSampleLabel() {
		if (fontName != null) {
			sampleLabel.setFont(new Font(fontName, fontStyle, size));
			if (sampleText != null) {
				sampleLabel.setText(" " + sampleText);
			}
		} else {
			sampleLabel.setText(" No font selected");
		}
	}


}
