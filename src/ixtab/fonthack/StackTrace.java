package ixtab.fonthack;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class StackTrace {

	public static String get(Throwable t) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		PrintWriter w = new PrintWriter(bytes);
		t.printStackTrace(w);
		w.close();
		return new String(bytes.toByteArray());
	}

}
