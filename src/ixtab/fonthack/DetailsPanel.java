package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources._detailspanel_description;
import static ixtab.fonthack.resources.FontHackResources._detailspanel_resetFontMessage;
import static ixtab.fonthack.resources.FontHackResources._detailspanel_resetFontTitle;
import static ixtab.fonthack.resources.FontHackResources._detailspanel_resetToDefaultFont;
import static ixtab.fonthack.resources.FontHackResources._detailspanel_setFontMessage;
import static ixtab.fonthack.resources.FontHackResources._detailspanel_setFontTitle;
import static ixtab.fonthack.resources.FontHackResources.i18n;
import ixtab.fonthack.FontInfo.Variant;
import ixtab.fonthack.FontSelectionChangedEvent.Listener;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.MessageFormat;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.ui.KOptionPane;

public class DetailsPanel extends JPanel implements ActionListener {

	private static final int COMPONENTS_FONT_SIZE = 10; //22;
	//7, 8, 9, 11, 13, 16, 25, 37
	private static final String[] SIZE_CHOICES = new String[] { " 7", " 8 ", " 9 ", " 11 ", " 13 ",
		" 16 ", " 25 ", " 37 " };
//	private static final String[] SIZE_CHOICES = new String[] { " 17 ", " 19 ", " 21 ", " 25 ", " 31 ",
//		" 36 ", " 60 ", " 86 " };

	private final JPanel headerPanel;
	private final DetailVariantPanel[] variantPanels;

	private static final long serialVersionUID = 5392489773535240303L;

	private final InfoProvider provider;
	private final JComboBox fontsComboBox;
	private final JComboBox sizesComboBox;
	private final JTextField sampleTextField;

	private final JButton[] setAsSystemFontButtons;
	private final JPanel setAsSystemFontsPanel;

	private final JButton resetToDefaultFontButton;
	private final MessageFormat resetToDefaultFontText = new MessageFormat(
			i18n(_detailspanel_resetToDefaultFont));
	private final JPanel resetToDefaultFontPanel;

	private JPanel footerPanel;

	public static String getDescription() {
		return i18n(_detailspanel_description);
	}

	public DetailsPanel(InfoProvider fontInfoProvider) {
		this.provider = fontInfoProvider;
		setLayout(new GridBagLayout());
		fontsComboBox = buildFontsComboBox();
		sizesComboBox = buildSizesComboBox();
		sampleTextField = buildSampleTextField();
		headerPanel = buildHeaderPanel();
		variantPanels = buildVariantPanels();
		setAsSystemFontButtons = buildSetAsSystemFontButtons();
		setAsSystemFontsPanel = buildSetAsSystemFontsPanel();
		resetToDefaultFontButton = buildResetToDefaultFontButton();
		resetToDefaultFontPanel = buildResetToDefaultFontPanel();
		footerPanel = buildFooterPanel();

		fontsComboBox.setSelectedIndex(0);
		sizesComboBox.setSelectedIndex(5);
		buildLayout();
	}

	private class FontSelectionListeningComboBox extends JComboBox implements Listener {
		private static final long serialVersionUID = -161254975749289251L;

		public FontSelectionListeningComboBox(Object[] items) {
			super(items);
			this.setFont(new Font(this.getFont().getName(), Font.PLAIN,
					COMPONENTS_FONT_SIZE));
			addActionListener(DetailsPanel.this);
			FontSelectionChangedEvent.addListener(this);
		}

		@Override
		public void onFontSelectionChangedEvent(FontSelectionChangedEvent event) {
			FontInfo after = event.fontInfo;
			FontInfo before = (FontInfo) this.getSelectedItem();
			if (before == null || after == null) {
				return;
			}
			if (!before.toString().equals(after.toString())) {
				boolean ok = false;
				int total = this.getModel().getSize();
				for (int i=0; i < total; ++i) {
					if (this.getItemAt(i).equals(after)) {
						this.setSelectedItem(this.getItemAt(i));
						ok = true;
						break;
					}
				}
				if (!ok) {
					// this probably means something went wrong before, not here.
					// throw new RuntimeException("Whatever");
				}
			}
		}
		
	}
	private JComboBox buildFontsComboBox() {
		JComboBox result = new FontSelectionListeningComboBox(
				provider.getFontInfoArray(InfoProvider.LISTMODE_USABLE));
		return result;
	}

	private JComboBox buildSizesComboBox() {
		// auto-boxing doesn't seem to work with retrotranslator
		JComboBox result = new JComboBox(SIZE_CHOICES);
		result.setFont(new Font(result.getFont().getName(), Font.PLAIN,
				COMPONENTS_FONT_SIZE));
		result.addActionListener(this);
		return result;
	}

	private JTextField buildSampleTextField() {
		final JTextField result = new JTextField(
				DetailVariantPanel.DEFAULT_SAMPLE_TEXT, 30);
		result.setFont(new Font(result.getFont().getName(), Font.PLAIN,
				COMPONENTS_FONT_SIZE));

		// this entire procedure is a bit hacky and awkward, but it seems to
		// work.
		result.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					// ENTER pressed: close on-screen keyboard by directing
					// focus away.
					sizesComboBox.requestFocus();
				}
			}

			@Override
			public void keyTyped(final KeyEvent e) {
				// key typed: enqueue new event. If we directly invoke the
				// ActionListener,
				// we may get missing characters in the output.
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						DetailsPanel.this.actionPerformed(new ActionEvent(
								result, 0, null));
					}
				});
			}

		});
		return result;
	}

	private JPanel buildHeaderPanel() {
		JPanel panel = new JPanel(new BorderLayout(8, 8));
		// panel.add(new JLabel(i18n(_fontdetails_fonts_label)),
		// BorderLayout.WEST);
		JPanel panel2 = new JPanel(new BorderLayout(8, 8));
		panel2.add(new JLabel(""), BorderLayout.WEST);
		panel2.add(fontsComboBox, BorderLayout.CENTER);
		panel2.add(sizesComboBox, BorderLayout.EAST);
		panel.add(panel2, BorderLayout.WEST);
		panel.add(sampleTextField, BorderLayout.CENTER);
		panel.add(new JLabel(""), BorderLayout.EAST);
		return panel;
	}

	private DetailVariantPanel[] buildVariantPanels() {
		DetailVariantPanel[] variants = new DetailVariantPanel[FontInfo.VARIANTS.length];
		for (int i = 0; i < variants.length; ++i) {
			variants[i] = buildVariantPanel(FontInfo.VARIANTS[i]);
		}
		return variants;
	}

	private DetailVariantPanel buildVariantPanel(Variant variant) {
		return new DetailVariantPanel(variant);
	}

	private JButton[] buildSetAsSystemFontButtons() {
		JButton[] buttons = new JButton[SystemFontInfo.DESCRIPTORS.length];
		for (int i = 0; i < buttons.length; ++i) {
			buttons[i] = buildSetAsSystemFontButton(i);
		}
		return buttons;
	}

	private JPanel buildSetAsSystemFontsPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2, 2, 5, 5));
		for (JButton button : setAsSystemFontButtons) {
			panel.add(button);
		}
		return panel;
	}

	private JButton buildSetAsSystemFontButton(final int index) {
		final String font = SystemFontInfo.DESCRIPTORS[index].javaName;
		JButton button = new JButton("⇒" + font);
		
		//button.setFont(new Font(font, Font.BOLD, BUTTONS_FONT_SIZE));
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FontInfo replacement = (FontInfo) fontsComboBox
				.getSelectedItem();
				String message = MessageFormat.format(
						i18n(_detailspanel_setFontMessage), font, replacement.toString().trim());
				int result = KOptionPane.showConfirmDialog(DetailsPanel.this,
						message, i18n(_detailspanel_setFontTitle),
						KOptionPane.CANCEL_OK_OPTION);
				if (result == KOptionPane.OK_OPTION) {
					if (replacement instanceof HackFontInfo) {
						provider.overrideWithHackFont(DetailsPanel.this, SystemFontInfo.DESCRIPTORS[index], (HackFontInfo) replacement);
					}
				}
			}
		});
		return button;
	}

	private JButton buildResetToDefaultFontButton() {
		JButton button = new JButton(
				resetToDefaultFontText.format(new Object[] { "?" }));
		button.setFont(new Font(button.getFont().getName(), Font.BOLD,
				COMPONENTS_FONT_SIZE));
		return button;
	}

	private JPanel buildResetToDefaultFontPanel() {
		JPanel panel = new JPanel(new BorderLayout(10, 10));
		panel.add(new JLabel(""), BorderLayout.WEST);
		panel.add(resetToDefaultFontButton, BorderLayout.CENTER);
		panel.add(new JLabel(""), BorderLayout.EAST);

		resetToDefaultFontButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// no regexes on Kindle :-(
				String font = fontsComboBox.getSelectedItem().toString().trim();
				if (font.startsWith(SystemFontInfo.MARKER)) {
					font = font.substring(SystemFontInfo.MARKER.length()).trim();
				}
				String message = MessageFormat.format(
						i18n(_detailspanel_resetFontMessage), font);
				int result = KOptionPane.showConfirmDialog(DetailsPanel.this,
						message, i18n(_detailspanel_resetFontTitle),
						KOptionPane.CANCEL_OK_OPTION);
				if (result == KOptionPane.OK_OPTION) {
					provider.resetSystemFont(DetailsPanel.this, (FontInfo)fontsComboBox.getSelectedItem());
				}
			}
		});
		return panel;
	}

	private JPanel buildFooterPanel() {
		return new FooterPanel(setAsSystemFontsPanel);
	}

	private void buildLayout() {
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		add(headerPanel, gbc);
		for (JPanel panel : variantPanels) {
			add(panel, gbc);
		}
		add(footerPanel, gbc);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = null;
		if (e.getSource().equals(fontsComboBox)) {
			source = fontsComboBox;
		} else if (e.getSource().equals(sizesComboBox)) {
			source = sizesComboBox;
		} else if (e.getSource().equals(sampleTextField)) {
			source = sampleTextField;
		}

		if (source != null) {
			FontInfo fi = (FontInfo) fontsComboBox.getSelectedItem();
			Integer size = new Integer(Integer.parseInt(((String) sizesComboBox
					.getSelectedItem()).trim()));
			String sample = sampleTextField.getText();
			new FontSelectionChangedEvent(source, sample, size, fi).post();
		}
	}

	private class FooterPanel extends JPanel implements
			FontSelectionChangedEvent.Listener {
		private static final long serialVersionUID = 2287774523693515426L;

		private FooterPanel(JPanel childPanel) {
			this.setLayout(new BorderLayout());
			setChildPanel(childPanel);
			FontSelectionChangedEvent.addListener(this);
		}

		private void setChildPanel(JPanel childPanel) {
			this.removeAll();
			this.add(childPanel, BorderLayout.CENTER);
		}

		@Override
		public void onFontSelectionChangedEvent(FontSelectionChangedEvent event) {
			FontInfo info = event.fontInfo;
			if (info != null) {
				if (info.isHack()) {
					setChildPanel(setAsSystemFontsPanel);
					String font = info.getName();
					for (int i = 0; i < setAsSystemFontButtons.length; ++i) {
						setAsSystemFontButtons[i]
								.setEnabled(!font.equals(SystemFontInfo
										.retrieve(SystemFontInfo.DESCRIPTORS[i].internalName)));
					}
				} else {
					resetToDefaultFontButton.setText(resetToDefaultFontText
							.format(new Object[] { info.getName() }));
					resetToDefaultFontButton.setEnabled(provider.isSystemFontChanged(info.getName()));
					setChildPanel(resetToDefaultFontPanel);
				}
			}
		}
	}

}
