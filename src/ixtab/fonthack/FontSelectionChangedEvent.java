package ixtab.fonthack;

import java.util.ArrayList;
import java.util.List;

public class FontSelectionChangedEvent {
	
	public final Object source;
	public final String sampleText;
	public final Integer size;
	public final FontInfo fontInfo;
	
	public interface Listener {
		void onFontSelectionChangedEvent(FontSelectionChangedEvent event);
	}
	
	private static List<Listener> listeners = new ArrayList<Listener>();
	
	public static void addListener(Listener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}
	
	public FontSelectionChangedEvent(Object source, String sampleText,
			Integer size, FontInfo fi) {
		super();
		this.source = source;
		this.sampleText = sampleText;
		this.size = size;
		this.fontInfo = fi;
		
	}
	
	public void post() {
		if (!fontInfo.isValid()) {
			// do nothing for invalid fonts.
			return;
		}
		synchronized (listeners) {
			for (Listener listener: listeners) {
				if (!listener.equals(source)) {
					listener.onFontSelectionChangedEvent(this);
				}
			}
		}
	}
	
}
