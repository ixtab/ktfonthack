package ixtab.fonthack;

import ixtab.fonthack.SystemFontInfo.Descriptor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FontConfig {
	public static final String INCLUSION_TEMPLATE="resources/fc-inc.xml";
	public static final String OVERRIDE_TEMPLATE="resources/fc-ovr.xml";
	private static final String PATTERN_DIR="$DIR";
	private static final String PATTERN_FONT="$FONT";
	private static final String PATTERN_SYSTEM="$SYSTEM";
	private static final File BASE_DIR = HackFontInfo.FONTINFO_DIRECTORY;
	
	public static boolean createOverride(Descriptor system,
			HackFontInfo font) throws IOException {
		List<File> output = getFileForDescriptor(system);
		
		String[][] replacements = new String[][] {
				new String[] {PATTERN_SYSTEM, system.defaultFont},
				new String[] {PATTERN_FONT, font.getName()}
		};
		createFiles(output, OVERRIDE_TEMPLATE, replacements);
		return true;
	}

	public static boolean createDefaultInclusionFile(HackFontInfo font) throws IOException {
		File output = font.getConfigurationFile();
		
		String[][] replacements = new String[][] {
				new String[] {PATTERN_DIR, font.getDirectory().getAbsolutePath()},
				new String[] {PATTERN_FONT, font.getName()}
		};
		
		createFile(output, INCLUSION_TEMPLATE, replacements);
		
		return true;
	}

	public static void createFiles(List<File> outputs, String templateResourceName,
			String[][] replacements) throws IOException, FileNotFoundException {
		for (File file: outputs) {
			createFile(file, templateResourceName, replacements);
		}
	}
	
	public static void createFile(File output, String templateResourceName,
			String[][] replacements) throws IOException, FileNotFoundException {
		
		BufferedReader template = getBufferedReaderForResource(templateResourceName);
		PrintWriter out = new PrintWriter(new FileOutputStream(output));
		for (String line=template.readLine(); line != null; line=template.readLine()) {
			for (String[] replace : replacements) {
				line = NoRegex.replaceAll(line, replace[0], replace[1]);
			}
			out.println(line);
		}
		template.close();
		out.close();
	}

	private static BufferedReader getBufferedReaderForResource(
			String template) throws IOException {
		InputStream is = FontConfig.class.getResourceAsStream(template);
		if (is == null) {
			throw new IOException(template+" not found");
		}
		return new BufferedReader(new InputStreamReader(is));
	}

	public static boolean removeOverride(Descriptor desc) {
		for (File file: getFileForDescriptor(desc)) {
			if (file.exists()) {
				file.delete();			
			}
		}
		return true;
	}

	private static List<File> getFileForDescriptor(Descriptor desc) {
		List<File> files = new ArrayList<File>();
		String filePrefix = BASE_DIR.getAbsolutePath();
		if (!filePrefix.endsWith(File.separator)) {
			filePrefix += File.separator;
		}
		files.add(new File(filePrefix+ "99-"+desc.internalName+".conf"));
		if (desc.needsTwoFiles()) {
			files.add(new File(filePrefix+ "98-"+desc.internalName+".conf"));
		}
		return files;
	}


}
