package ixtab.fonthack;

import static ixtab.fonthack.resources.FontHackResources.*;

import ixtab.fonthack.CommandLine.Result;
import ixtab.fonthack.resources.FontHackResources;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.ui.KOptionPane;

public class SystemPanel extends JPanel {

	private static final long serialVersionUID = 2536364089885255120L;
	private static final String[] FC_CACHE_COMMAND= new String[] {"/usr/bin/fc-cache", "-f" ,"-v"};

	public static String getDescription() {
		return i18n(_systempanel_description);
	}

	private final JButton rebuildButton;
	private final JButton restartButton;
	private final JTextArea result;
	
	public SystemPanel(InfoProvider fontInfoProvider) {
		result = new JTextArea(20, 20);
		result.setEditable(false);
		result.setText(i18n(_systempanel_command_output_description));
		
		rebuildButton = new JButton(i18n(_systempanel_regenerate_cache_button));
		restartButton = new JButton(i18n(_systempanel_restart_framework_button));
		rebuildButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				regenerateFontCache();
			}
		});
		
		restartButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Reset.perform(SystemPanel.this);
			}
		});
		
		
		initUI();
	}

	private void initUI() {
		//this.setLayout(new GridLayout(2, 1));
		this.setLayout(new BorderLayout());
		JPanel top = new JPanel(new BorderLayout(5,5));
		
		JPanel buttonWrapper = new JPanel(new BorderLayout(5,5));
		buttonWrapper.add(rebuildButton, BorderLayout.WEST);
		buttonWrapper.add(restartButton, BorderLayout.EAST);
		buttonWrapper.setBorder(BorderFactory.createLineBorder(Color.WHITE, 5));
		top.add(buttonWrapper, BorderLayout.NORTH);
		
		result.setFont(new Font("Monospace", Font.PLAIN, result.getFont().getSize() * 3/4 ));
		JScrollPane resultWrapper = new JScrollPane(result);
		resultWrapper.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2), " " + i18n(_systempanel_command_output_label) +" "));
		top.add(resultWrapper, BorderLayout.CENTER);
		
		//top.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK, 2), i18n(_systempanel_actions_label)));
		
		JPanel bottom = initBottomPanel();
		
		this.add(top, BorderLayout.CENTER);
		this.add(bottom, BorderLayout.SOUTH);
		
		this.setBorder(BorderFactory.createLineBorder(Color.WHITE, 10));
	}

	private JPanel initBottomPanel() {
		JPanel bottom = new JPanel();
		bottom.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY, 2), " " + i18n(_systempanel_about_label)+ " "));
		
		List<JLabel> labels = new ArrayList<JLabel>();
		labels.add(new JLabel("   " + i18n(_systempanel_about_version)+ ": " +Metadata.VERSION));
		labels.add(new JLabel("   " + i18n(_systempanel_about_author)+ ": " +Metadata.AUTHOR));
		String translator = i18n(_systempanel_about_translation_author);
		if (translator != null && translator.trim().length() != 0 && (!translator.trim().equals(FontHackResources.DEFAULT_TRANSLATOR_INFO))) {
			labels.add(new JLabel("   " +i18n(_systempanel_about_translation_key)+ ": " + translator ));
		}
		
		bottom.setLayout(new GridLayout(labels.size(), 1));
		for (JLabel label: labels) {
			bottom.add(label);
		}
		
		return bottom;
	}

	private void regenerateFontCache() {
		rebuildButton.setEnabled(false);
		final StringBuffer sb = new StringBuffer();
		new Thread() {
			@Override
			public void run() {
				try {
					Result running = CommandLine.exec(false, FC_CACHE_COMMAND);
					InputStream is = running.getStdOutStream();
					for (int i = is.read(); i != -1; i=is.read()) {
						char c = (char) i;
						sb.append(c);
						setResultFrom(sb);
					}
					// stream is closed, so process is done.
					int code = running.process.exitValue();
					if (code != 0) {
						commandFinished(new IOException("Process finished with return code " + code));
					}
					commandFinished(null);
				} catch (IOException io) {
					commandFinished(io);
				}
			}
		}.start();
	}

	private void setResultFrom(StringBuffer sb) {
		final String text = sb.toString();
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				result.setText(text);
			}
		});
	}

	private void commandFinished(final IOException exception) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				rebuildButton.setEnabled(true);
				if (exception != null) {
					KOptionPane.showMessageDialog(SystemPanel.this, StackTrace.get(exception), i18n(_systempanel_command_execution_failed));
				} else {
					new FontEvent(FontEvent.Listener.REBUILD_DONE).post();
				}
			}
		});
	}



}
