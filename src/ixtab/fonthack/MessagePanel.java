package ixtab.fonthack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import static ixtab.fonthack.resources.FontHackResources.*;

public class MessagePanel extends JPanel implements FontEvent.Listener {
	
	private static final long serialVersionUID = -8161828703841026050L;
	
	private final JLabel rebuildLabel;
	private final JLabel restartLabel;
	
	public MessagePanel() {
		FontEvent.addListener(this);
		setLayout(new BorderLayout());
		
		MouseAdapter click = new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				new FontEvent(SHOW_SYSTEM_TAB).post();
			}
			
		};
		rebuildLabel = new JLabel(" ▶ " + i18n(_messagepanel_rebuild_label));
		restartLabel = new JLabel(" ▶ " + i18n(_messagepanel_restart_label));
		rebuildLabel.addMouseListener(click);
		restartLabel.addMouseListener(click);
		
		add(rebuildLabel, BorderLayout.NORTH);
		add(restartLabel, BorderLayout.SOUTH);
		setBorder(BorderFactory.createLineBorder(Color.WHITE, 5));
		
		rebuildLabel.setVisible(false);
		restartLabel.setVisible(false);
	}

	@Override
	public void onFontEvent(FontEvent event) {
		if ((event.type & RESTART_REQUIRED) == RESTART_REQUIRED) {
			restartLabel.setVisible(true);
		}
		if ((event.type & REBUILD_REQUIRED) == REBUILD_REQUIRED) {
			rebuildLabel.setVisible(true);
		}
		if ((event.type & REBUILD_DONE) == REBUILD_DONE) {
			rebuildLabel.setVisible(false);
			new FontEvent(RESTART_REQUIRED).post();
		}
	}
}
