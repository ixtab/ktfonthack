package ixtab.fonthack.resources;

import java.net.URL;

public class Icons {

	public static final String CONDENSED ="condensed";
	public static final String INVALID ="invalid";
	public static final String MONOSPACE ="monospace";
	public static final String SANS_SERIF ="sansserif";
	public static final String SERIF ="serif";
	public static final String UNINITIALIZED ="uninitialized";
	
	public static URL getUrl(String resource) {
		return Icons.class.getResource(resource);
	}

}
