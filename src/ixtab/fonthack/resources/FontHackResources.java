package ixtab.fonthack.resources;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class FontHackResources extends ListResourceBundle {
	public static final String DEFAULT_TRANSLATOR_INFO = "@translator: YOUR_NAME_OR_PSEUDONYM. Leave empty or at the default if you don't want this information to be displayed.";
	
	
	public static final String[] _overviewpanel_description = new String[] { "_overviewpanel_description", "Overview" };
	public static final String[] _overviewpanel_no_fonts_label = new String[] { "_overviewpanel_no_fonts_label", "No fonts found in directory «fonthack»." };
	public static final String[] _overviewpanel_invalid_title = new String[] { "_overviewpanel_invalid_title", "Invalid Font" };
	public static final String[] _overviewpanel_invalid_message = new String[] { "_overviewpanel_invalid_message", "The directory «{0}» is not a valid font directory." };
	public static final String[] _overviewpanel_uninitialized_title = new String[] { "_overviewpanel_uninitialized_title", "Uninitialized Font" };
	public static final String[] _overviewpanel_uninitialized_message = new String[] { "_overviewpanel_uninitialized_message", "The font «{0}» is not properly initialized and may not be fully usable on your device.\n\nDo you want to initialize this font now?" };

	public static final String[] _detailspanel_description = new String[] { "_detailspanel_description", "Details" };
	public static final String[] _detailspanel_resetToDefaultFont = new String[] { "_detailspanel_resetToDefaultFont", "Reset «{0}» to default" };
	public static final String[] _detailspanel_setFontTitle = new String[] { "_detailspanel_setFontTitle", "Override Font?" };
	public static final String[] _detailspanel_setFontMessage = new String[] { "_detailspanel_setFontMessage", "Do you want to set the «{0}» font to «{1}»?"};
	public static final String[] _detailspanel_resetFontTitle = new String[] { "_detailspanel_resetFontTitle", "Reset Font to Default?" };
	public static final String[] _detailspanel_resetFontMessage = new String[] { "_detailspanel_resetFontMessage", "Do you want to reset the «{0}» font?"};
	public static final String[] _detailspanel_sampleText = new String[] { "_detailspanel_sampleText", "Sample text"};
	
	public static final String[] _messagepanel_rebuild_label = new String[] { "_messagepanel_rebuild_label", "The font cache must be rebuilt." };
	public static final String[] _messagepanel_restart_label = new String[] { "_messagepanel_restart_label", "The framework must be restarted." };
	
	public static final String[] _systempanel_description = new String[] { "_systempanel_description", "System" };
	public static final String[] _systempanel_regenerate_cache_button = new String[] { "_systempanel_regenerate_cache_button", "Rebuild font cache" };
	public static final String[] _systempanel_restart_framework_button = new String[] { "_systempanel_restart_framework_button", "Restart framework" };
	public static final String[] _systempanel_command_output_label = new String[] { "_systempanel_command_output_label", "Program output" };
	public static final String[] _systempanel_command_output_description = new String[] { "_systempanel_command_output_description", "This box will display the program output\nwhile the font cache is rebuilt.\n\nNOTE: Rebuilding the font cache may take several minutes!" };
	public static final String[] _systempanel_about_label = new String[] { "_systempanel_about_label", "About Font Hack" };
	public static final String[] _systempanel_about_version = new String[] { "_systempanel_about_version", "Version" };
	public static final String[] _systempanel_about_author = new String[] { "_systempanel_about_author", "Author" };
	public static final String[] _systempanel_about_translation_key = new String[] { "_systempanel_about_translation_key", "YOUR_LANGUAGE translation" };
	public static final String[] _systempanel_about_translation_author = new String[] { "_systempanel_about_translation_author", DEFAULT_TRANSLATOR_INFO };
	public static final String[] _systempanel_command_execution_failed = new String[] { "_systempanel_command_execution_failed", "Execution failed!" };
		
	
	public static final String[] _reset_title = new String[] { "_reset_title", "Restart framework?" };
	public static final String[] _reset_message = new String[] { "_reset_message", "Do you want to restart the framework now?" };
	
	public static final String[] _error_title = new String[] { "_error_title", "Error" };
	public static final String[] _error_message = new String[] { "_error_message", "An unexpected error occurred." };
	
	public static final String[] _kindlet_jailbreak_unsuccessful_title = new String[] { "_kindlet_jailbreak_unsuccessful_title", "Kindlet Jailbreak error" };
	public static final String[] _kindlet_jailbreak_unsuccessful_message = new String[] { "_kindlet_jailbreak_unsuccessful_message", "The Kindlet Jailbreak is installed, but not all required permissions could be obtained." };
	public static final String[] _ = new String[] { "", "" };
	
	private static final MessageFormat localePattern = new MessageFormat("/mnt/us/fonthack/translation-{0}.properties");
	
	// order is important here!
	private static Object[][] DEFAULT_CONTENTS = getStaticContents();
	private static ResourceBundle bundle = findBundle();
	
	public static String i18n(String[] key) {
		return bundle.getString(key[0]);
	}
	
	private static Object[][] getStaticContents() {
		try {
			Field[] fields = FontHackResources.class.getDeclaredFields();
			List<Object[]> relevantFields = new ArrayList<Object[]>();
			for (Field field: fields) {
				if (field.getName().startsWith("_") && field.getName().length() > 1 && field.getType().equals(String[].class) && field.getModifiers() == (Modifier.PUBLIC | Modifier.STATIC | Modifier.FINAL)) {
					relevantFields.add(new Object[] {field.getName(), ((String[]) field.get(null))[1]});
					if (!field.getName().equals(((String[]) field.get(null))[0])) {
						throw new IllegalStateException("ixtab, watch your code: "+field.getName());
					}
				}
			}
			return relevantFields.toArray(new Object[relevantFields.size()][]);
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
//		return new Object[][] {
//				new Object[] { _fontdetails_description, "Details"},
//				new Object[] { _fontoverview_description, "Overview"},
//		};
	}

	@Override
	protected Object[][] getContents() {
		return DEFAULT_CONTENTS;
	}

	private static ResourceBundle findBundle() {
		ResourceBundle english = ResourceBundle.getBundle(FontHackResources.class.getName());
		
		Locale locale = Locale.getDefault();
		File localized = new File(localePattern.format(new Object[] { locale.toString() }));
		if (!localized.isFile()) {
			return english;
		}
		if (localized.length() == 0) {
			initializeLocalizationFile(localized, english);
		}
		return new PropertiesResourceBundle(localized, english);
	}
	
	private static void initializeLocalizationFile(File localized,
			ResourceBundle english) {
		TreeMap<String, String> props = new TreeMap<String, String>();
		Enumeration<String> keys = english.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			String value = english.getString(key);
			props.put(key, value);
		}
		try {
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(localized), "UTF-8"));
			writer.println("# This is a translation file for the font hack.");
			writer.println("# Empty lines, and lines starting with a # (such as this one) are ignored.");
			writer.println("# To simplify the translation, each entry is preceded by the complete english entry,");
			writer.println("# using a comment. Feel free to remove these comments, and empty lines, once your translation is finished.");
			writer.println();
			writer.println();
			for (Map.Entry<String, String> entry: props.entrySet()) {
				String line = entry.getKey()+"="+escape(entry.getValue());
				writer.println("#"+line);
				writer.println(line);
				writer.println();
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		};
	}

	private static String escape(String input) {
		StringBuffer output = new StringBuffer();
		for (char c: input.toCharArray()) {
			switch (c) {
			case '\\': output.append("\\\\"); break;
			case '\t': output.append("\\t"); break;
			case '\f': output.append("\\f"); break;
			case '\n': output.append("\\n"); break;
			case '\r': output.append("\\r"); break;
			case '#': output.append("\\#"); break;
			case '!': output.append("\\!"); break;
			case '=': output.append("\\="); break;
			case ':': output.append("\\:"); break;
			default: output.append(c);
			}
		}
		return output.toString();
	}

	private static byte[] convertEncoding(byte[] bytes, String sourceEncoding,
			String targetEncoding) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes), sourceEncoding));
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(result, targetEncoding));
		boolean escape = !targetEncoding.equals("UTF-8");
		for (String line = reader.readLine(); line != null; line = reader.readLine()) {
			for (char c: line.toCharArray()) {
				String o = "" + c;
				if (escape && (c < '\u0020' || c > '\u007e')) {
					int i = c;
					String hex = Integer.toHexString(i);
					while (hex.length() < 4) {
						hex = "0" + hex;
					}
					o = "\\u" + hex;
				}
				writer.write(o);
			}
			//bWriter.write(line);
			writer.newLine();
		}
		writer.flush();
		writer.close();
		result.flush();
		result.close();
		return result.toByteArray();
	}

	private static class PropertiesResourceBundle extends ResourceBundle {

		private final Properties props;
		
		public PropertiesResourceBundle(File file, ResourceBundle parent) {
			setParent(parent);
			props = loadProperties(file);
		}
		
		private Properties loadProperties(File file) {
			Properties props = new Properties();
			try {
				byte[] bytes = readRawBytes(file);
				bytes = convertEncoding(bytes, "UTF-8", "ISO-8859-1");
				props.load(new ByteArrayInputStream(bytes));
			} catch (IOException e) {};
			return props;
		}

		private byte[] readRawBytes(File file) throws IOException {
			byte[] result = new byte[(int)file.length()];
			InputStream is = new BufferedInputStream(new FileInputStream(file));
			int offset = 0;
			while (true) {
				int read = is.read(result, offset, result.length - offset);
				offset += read;
				if (read == -1 || offset == result.length) {
					break;
				}
			}
			return result;
		}

		@Override
		protected Object handleGetObject(String key) {
			return props.get(key);
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public Enumeration getKeys() {
			return props.keys();
		}
		
	}
}
