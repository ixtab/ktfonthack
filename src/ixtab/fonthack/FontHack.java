package ixtab.fonthack;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class FontHack {
	private final Container rootContainer;
	private final InfoProvider fontInfoProvider;
	
	public FontHack(Container rootContainer, InfoProvider fontInfoProvider) {
		super();
		this.rootContainer = rootContainer;
		this.fontInfoProvider = fontInfoProvider;
	}
	
	class TabsPane extends JTabbedPane implements FontSelectionChangedEvent.Listener, FontEvent.Listener {
		private static final long serialVersionUID = -6376854039364544102L;

		public TabsPane() {
			addTab(OverviewPanel.getDescription(), new OverviewPanel(fontInfoProvider));
			addTab(DetailsPanel.getDescription(), new DetailsPanel(fontInfoProvider));
			addTab(SystemPanel.getDescription(), new SystemPanel(fontInfoProvider));
			setSelectedIndex(0);
			FontSelectionChangedEvent.addListener(this);
			FontEvent.addListener(this);
		}

		@Override
		public void onFontSelectionChangedEvent(FontSelectionChangedEvent event) {
			setSelectedIndex(1);
		}

		@Override
		public void onFontEvent(FontEvent event) {
			if ((event.type & FontEvent.Listener.SHOW_SYSTEM_TAB) == FontEvent.Listener.SHOW_SYSTEM_TAB) {
				setSelectedIndex(2);
			}
		}
	}
	
	public void create() {
		rootContainer.setLayout(new BorderLayout());
		rootContainer.add(new TabsPane(), BorderLayout.CENTER);
		rootContainer.add(new MessagePanel(), BorderLayout.NORTH);
		rootContainer.validate();
	}
	
	public void addFontPanel(JPanel panel, String sampleText, int sampleSize,
			String fontName, String label) {
		panel.add(new JLabel(label));
		panel.add(new JLabel("Regular:"));
		panel.add(makeLabel(sampleText, fontName, sampleSize, Font.PLAIN));
		panel.add(new JLabel("Bold:"));
		panel.add(makeLabel(sampleText, fontName, sampleSize, Font.BOLD));
		panel.add(new JLabel("Italic:"));
		panel.add(makeLabel(sampleText, fontName, sampleSize, Font.ITALIC));
		panel.add(new JLabel("Bold Italic:"));
		panel.add(makeLabel(sampleText, fontName, sampleSize, Font.BOLD | Font.ITALIC));
	}

	private Component makeLabel(String sampleText, String fontName,
			int size, int style) {
		JLabel result = new JLabel(sampleText);
		result.setFont(new Font(fontName, style, size));
		return result;
	}
	
}
