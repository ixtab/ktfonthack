package ixtab.fonthack;

public class NoRegex {

	public static String replaceAll(String line, String pattern, String replacement) {
		int start = line.indexOf(pattern);
		if (start == -1) {
			return line;
		}
		
		String prefix = line.substring(0, start);
		String suffix = line.substring(start+pattern.length());
		String fixed = prefix + replacement +suffix;
		return replaceAll(fixed, pattern, replacement);
	}

}
